# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://aleksejt@bitbucket.org/aleksejt/stroboskop.git
git status
```

Naloga 6.2.3:
https://bitbucket.org/aleksejt/stroboskop/commits/f1c4a820729d433320357941d5e964230342e22b

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/aleksejt/stroboskop/commits/399056a683ccaecc092cbba00d5a616ef6f062f4

Naloga 6.3.2:
https://bitbucket.org/aleksejt/stroboskop/commits/09be01116270442d4518d2f1532d35cef95529b5

Naloga 6.3.3:
https://bitbucket.org/aleksejt/stroboskop/commits/4f82482df0f23dec1461590e5040cf81cdd5e10e

Naloga 6.3.4:
https://bitbucket.org/aleksejt/stroboskop/commits/ad5bde93929dfb9cdb3f2cf7f1b082923f6a5f48

Naloga 6.3.5:

```
git checkout master
git merge origin izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/aleksejt/stroboskop/commits/189b40332306cf77de4437c33c4a72947dbaff17

Naloga 6.4.2:
https://bitbucket.org/aleksejt/stroboskop/commits/4bf41bbd0d7a368ec248ec0105ef01e0f39af62a

Naloga 6.4.3:
https://bitbucket.org/aleksejt/stroboskop/commits/80b498eb2b33d33251d19c53a97a76d9dc36ca45

Naloga 6.4.4:
https://bitbucket.org/aleksejt/stroboskop/commits/d03befde54abfc49939f489249bd81278546ca77